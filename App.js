import './src/Config/ReactotronConfig';
import React from "react";
import { AppLoading } from 'expo';
import App from "./src/index";

export default class RootApp extends React.Component {
  constructor() {
    super();
      this.state = {
        isReady: false
      };
    }

    async componentWillMount() {
      await Expo.Font.loadAsync({
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
        Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
        'Mosk-Bold-700': require('./assets/fonts/Mosk-Bold-700.ttf'),
        'Mosk-Extra-Bold-800': require('./assets/fonts/Mosk-Extra-Bold-800.ttf'),
        'Mosk-Extra-Light-200': require('./assets/fonts/Mosk-Extra-Light-200.ttf'),
        'Mosk-Light-300': require('./assets/fonts/Mosk-Light-300.ttf'),
        'Mosk-Medium-500': require('./assets/fonts/Mosk-Medium-500.ttf'),
        'Mosk-Normal-400': require('./assets/fonts/Mosk-Normal-400.ttf'),
        'Mosk-Semi-Bold-600': require('./assets/fonts/Mosk-Semi-Bold-600.ttf'),
        'Mosk-Thin-100': require('./assets/fonts/Mosk-Thin-100.ttf'),
        'Mosk-Ultra-Bold-900': require('./assets/fonts/Mosk-Ultra-Bold-900.ttf'),
      });
      this.setState({ isReady: true });
    }
  
    render() {
      if (!this.state.isReady) {
        return <AppLoading />;
      }
      return <App />;
    }
  }
  