import React from "react";
import { View, Item, Input, Textarea, Label, Text } from 'native-base';
import styles from './Styles/FormTextareaStyles';
import DatePicker from 'react-native-datepicker'

export default class DatePickerInput extends React.Component {

  constructor(props){
    super(props)
    this.state = { date:"2016-05-15"}

    this._dateChange = this._dateChange.bind(this);
  }

  _dateChange(date) {
    this.setState({date})
  }

  render(){
    const { input: { onChange, value, ...inputProps }, label, last, meta: { touched, error, warning } } = this.props;
    
    let hasError = false;
    if (touched && error !== undefined) {
      hasError = true;
    }
  
    let isLast = false;
    if(last) {
      isLast = true;
    }
  
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <Item error={hasError} last={isLast} style={[styles.formInput, {borderColor: hasError ? "#ed2f2f" : null}]}>
          <DatePicker
            date={value}
            style={{flex: 1}}
            mode="date"
            placeholder={label}
            format="YYYY-MM-DD"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0
              },
              dateInput: {
                marginLeft: 40,
                borderWidth: null,
                alignItems: 'flex-start',
                height: 50,
              },
              placeholderText: {
                color: '#c9c9c9',
                fontSize: 18,
                fontFamily: "Mosk-Normal-400",
              },
              dateText: {
                fontSize: 18,
                fontFamily: "Mosk-Normal-400",
                color: '#4e3b5b',
              },
              btnTextText: {
                fontSize: 16,
                color: '#7e09c9'
              },
              // ... You can check the source to find the other keys.
            }}
            onDateChange={(value)=> {
              this._dateChange(value);
              return onChange(value);
            }}
          />
        </Item>
        {hasError ? <Item style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", borderBottomWidth: null  }}>
          <Text style={{ fontSize: 14, color: "#ed2f2f", marginTop: 5, }} uppercase>{error}</Text>
        </Item> : <Text/>}
      </View>
    ); 
  }
} 

