import React from "react";
import { View, Item, Input, Text, Label, Icon } from 'native-base';
import styles from './Styles/FormInputStyles';


const FormInput = (props) => {
  const {input, label, icon, last, type, meta: { touched, error, warning , submitting }, ...inputProps} = props;
  
  let hasError = false;
  if (touched && error !== undefined) {
    hasError = true;
  }

  let isLast = false;
  if(last) {
    isLast = true;
  }

  return (
    <View>
      <Item error={hasError} last={isLast} style={[styles.formInput, {borderColor: hasError ? "#ed2f2f" : null}]}>
        <Icon name={icon} />
        <Input disabled={submitting} placeholder={label} {...input} {...inputProps} style={styles.input}/>
      </Item>
      {hasError ? <Item style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", borderBottomWidth: null  }}>
        <Text style={{ fontSize: 14, color: "#ed2f2f", marginTop: 5, }} uppercase>{error}</Text>
      </Item> : <Text/>}
    </View>
  ); 
}

export default FormInput;