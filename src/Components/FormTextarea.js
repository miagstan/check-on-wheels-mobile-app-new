import React from "react";
import { View, Item, Input, Textarea, Label, Text } from 'native-base';
import styles from './Styles/FormTextareaStyles';
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput';



const FormTextarea = (props) => {
  const {input, label, last, type, meta: { touched, error, warning , submitting }, ...inputProps} = props;
  
  let hasError = false;
  if (touched && error !== undefined) {
    hasError = true;
  }

  let isLast = false;
  if(last) {
    isLast = true;
  }

  return (
    <View style={{flex: 1, flexDirection: 'column'}}>
      <View error={hasError} last={isLast} style={[styles.formInput, {borderColor: hasError ? "#ed2f2f" : null}]}>
        <AutoGrowingTextInput maxHeight={200}
            minHeight={200} disabled={submitting} placeholder={label} {...input} {...inputProps} style={styles.input}
          />
      </View>
      {hasError ? <Item style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", borderBottomWidth: null  }}>
        <Text style={{ fontSize: 14, color: "#ed2f2f", marginTop: 5, }} uppercase>{error}</Text>
      </Item> : <Text/>}
    </View>
  ); 
}

export default FormTextarea;