import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class If extends Component {
    render() {
        const {hide, condition, children} = this.props;
        let conditionFulfilled = typeof condition === 'function'
            ? condition()
            : condition;
        if (hide) 
            conditionFulfilled = !conditionFulfilled;
        conditionFulfilled = Boolean(conditionFulfilled);
        return conditionFulfilled
            ? children
            : null;
    }
}
If.propTypes = {
    children: PropTypes.node.isRequired,
    hide: PropTypes.bool,
    condition: PropTypes.oneOfType([PropTypes.bool, PropTypes.func])
};
If.defaultProps = {
    condition: true
};