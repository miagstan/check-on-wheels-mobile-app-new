const React = require("react-native");
const { StyleSheet, Platform } = React;

import platform from '../../../native-base-theme/variables/platform';


import { Metrics, Colors, Fonts } from '../../Themes/';

export default {
  formInput: {
    borderColor: Colors.fireLight,
  },
  input: {
    fontFamily: "Mosk-Normal-400",
    color: '#4e3b5b',
  }
}