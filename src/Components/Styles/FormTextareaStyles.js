const React = require("react-native");
const { StyleSheet, Platform, PixelRatio } = React;

import platform from '../../../native-base-theme/variables/platform';


import { Metrics, Colors, Fonts } from '../../Themes/';

export default {
  formInput: {
    borderColor: Colors.fireLight,
  },
  input: {
    fontSize: 18,
    fontFamily: "Mosk-Normal-400",
    borderColor: Colors.fireLight,
    borderBottomWidth:  1 / PixelRatio.getPixelSizeForLayoutSize(1),
    paddingVertical: 10,
    color: '#4e3b5b',
  },
  dateInput: {
    borderWidth: null,
    borderColor: null,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
}