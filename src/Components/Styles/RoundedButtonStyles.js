import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  button: {
    height: 45,
    borderRadius: 5,
    marginHorizontal: 25,
    marginVertical: 10,
    backgroundColor: '#e73536',
    justifyContent: 'center'
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    marginVertical: 10
  }
})