export default {
  useFixtures: false,
  ezLogin: false,
  yellowBox: __DEV__,
  reduxLogging: __DEV__,
  useReactotron: __DEV__,
  //host: '192.168.0.191',
  host: '10.17.50.127',
}
