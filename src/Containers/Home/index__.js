import React from 'react'
import { connect } from 'react-redux'
import { View, Text } from 'react-native'
import RoundedButton from '../../Components/RoundedButton'
import styles from './styles'
import AuthActions from '../../Redux/AuthRedux'

class Home extends React.Component {
  render () {
    const { navigate } = this.props.navigation
    return (
      <View style={styles.container}>
        <Text style={styles.headerText}>You are logged in</Text>
        <RoundedButton
          text='Go to Another Authenticated Screen'
          onPress={() => navigate('Profile')}
        />
        <RoundedButton text='Logout' onPress={this.props.logout} />
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(AuthActions.logout())
  }
}

export default connect(null, mapDispatchToProps)(Home)