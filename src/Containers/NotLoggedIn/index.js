import React, { Component } from "react";
import { Image } from "react-native";

import {
  Container,
  Content,
  Button,
  Icon,
  Text,
  H1,
  H2,
  H3,
  View,
} from "native-base";

import styles from "./styles";
const launchscreenLogo = require("../../Images/checkupvan.png");

class NotLoggedInPage extends Component {

  render() {
    const { navigation } = this.props;
    return (
      <Container style={styles.container}>
        <Content style={styles.content}>
          <View style={styles.headerContainer}>
            <H3 style={styles.version}>/BETA 1.0</H3>
            <View style={styles.titleContainer}>
              <Text style={styles.title1}>CHECKUP</Text>
            </View>          
            <View style={styles.titleContainer}>
              <Text style={styles.title2small}>ON </Text>
              <Text style={styles.title2}>WHEELS</Text>
            </View>
            <View style={styles.titleContainer}>
              <Text style={styles.slogan}>Mobile health checkup near you</Text>
            </View>
            <View style={styles.line} />
          </View>
          
          <View style={styles.logoContainer}>
            <Image source={launchscreenLogo} style={styles.logo} resizeMode="contain"/>
          </View>
          <View style={styles.buttonContainer}>
            <Button 
              primary 
              block 
              style={[styles.btn, styles.btnPrimary]}
              onPress={() => navigation.navigate("SignUp")}>
              <Text style={styles.btnText}>{'Create a new account'}</Text>
            </Button>
            <Button 
              light 
              block 
              style={[styles.btn, styles.btnSecondary]}
              onPress={() => navigation.navigate("SignIn")}>
              <Text style={styles.btnText}>{'Sign in to your account'}</Text>
            </Button>
          </View>
        </Content>
      </Container>
    )
  }
}

export default NotLoggedInPage;