const React = require("react-native");
const { StyleSheet, Platform } = React;

import { Metrics, Colors, Fonts } from '../../Themes/';

const logoWidth = Metrics.screenWidth - 90;
const aspectRatio = 1500 / 779;

export default {
  container: {
    backgroundColor: "#FFF",
  },
  content: {
    margin: 30,
    backgroundColor: Colors.themeBg,
  },
  headerContainer: {
    paddingLeft: 30,
    paddingTop: 40,
    marginBottom: 40,
  },
  version: {
    fontFamily: "Mosk-Thin-100",
    fontWeight: "100",
    fontSize: 18,
    color: Colors.fireLight,
    marginBottom: 30,
  },
  titleContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  title1: {
    fontFamily: "Mosk-Extra-Bold-800",
    color: Colors.fireLight,
    fontSize: 40,
  },
  title2: {
    fontFamily: "Mosk-Extra-Bold-800",
    color: Colors.fireLight,
    fontSize: 40,
  },
  title2small: {
    fontSize: 40,
    fontFamily: "Mosk-Normal-400",
    color: Colors.fireLight,
  },
  slogan: {
    fontSize: 15,
    fontFamily: "Mosk-Medium-500",
    color: Colors.fireLight,
  },
  line: {
    marginTop: 12,
    backgroundColor: Colors.snow,
    height: 8,
    width: Metrics.screenWidth / 3.5
  },
  logoContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "stretch",
    marginLeft: 30,
    marginBottom: 60
  },
  logo: {
    width: logoWidth,
    height: logoWidth / aspectRatio,
  },
  buttonContainer: {
    paddingHorizontal: 50,
  },
  btn: {
    borderRadius: null,
    marginBottom: 10,
  },
  btnText: {
    fontFamily: "Mosk-Medium-500",
    fontSize: 18,
  },
  btnPrimary: {
    backgroundColor: Colors.fireLight,
  },
  btnSecondary: {
    backgroundColor: Colors.snow,
  },
};