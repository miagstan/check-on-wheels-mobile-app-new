import React, { Component } from "react";
import { Platform, TextInput } from "react-native";
import PropTypes from 'prop-types';
import CountryPicker from 'react-native-country-picker-modal';
import { onSubmitActions } from 'redux-form-submit-saga';

import { connect } from "react-redux";
import {
  Item,
  Input,
  Button,
  View,
  Text,
  Spinner,
} from "native-base";
import { Field, reduxForm, submit } from "redux-form";

import styles from "./styles";
//import If from '../../components/If';
import validate from '../../Utils/validate';
import { Colors} from '../../Themes/';

// if you want to customize the country picker
const countryPickerCustomStyles = {};

class RequestForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      enterCode: false,
      spinner: false,
      country: {
        cca2: 'FI',
        callingCode: '358'
      }
    };

    this._renderCountryPicker = this._renderCountryPicker.bind(this);
    this._renderCallingCode = this._renderCallingCode.bind(this);
    this._changeCountry = this._changeCountry.bind(this);
    this._renderInput = this._renderInput.bind(this);
    
  }

  _renderInput(props) {
    const {input, label, type, meta: { touched, error, warning , submitting }, ...inputProps} = props;
  
    return (
      <TextInput
        {...input}
        {...inputProps}
        disabled={submitting}
        ref={(node) => this.textInput = node}
        type={'TextInput'}
        underlineColorAndroid={'transparent'}
        autoCapitalize={'none'}
        autoCorrect={false}
        placeholder={'Phone Number'}
        keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'numeric'}
        style={[ styles.textInput ]}
        returnKeyType='go'
        autoFocus
        placeholderTextColor={Colors.cBlack}
        selectionColor={Colors.cBlack}
        maxLength={20} />
    ); 
  }

  _changeCountry(country) {
    this.setState({ country });
    this.textInput.setNativeProps({ text: '' })
    this.textInput.focus();
  }

  _renderCountryPicker(props) {
    const { input: { onChange, value, ...inputProps }, meta: { touched, error, warning } } = props;
    return (
      <CountryPicker
        closeable
        style={styles.countryPicker}
        onChange={(value)=> {
          this._changeCountry(value);
          return onChange(value);
        }}
        cca2={this.state.country.cca2}
        styles={countryPickerCustomStyles}
        translation='eng'/>
    );
  }

  _renderCallingCode() {
    return (
      <View style={styles.callingCodeView}>
        <Text style={styles.callingCodeText}>+{this.state.country.callingCode}</Text>
      </View>
    );
  }

  render() {
    const { handleSubmit, error, submitting, submitForm } = this.props;
    return(
      <View ref={'form'} >
        <View style={{ flexDirection: 'row', marginBottom: 20 }}>
          <Field name="country" component={this._renderCountryPicker}/>
          {this._renderCallingCode()}
          <Field name="contactNumber" component={this._renderInput} />
        </View>
        <Button
          large 
          block 
          style={[styles.btn, styles.btnPrimary]}
          onPress={() => submitForm()}>
          {submitting ? <Spinner color="white" size="small"/> : <Text style={styles.btnText}>{ 'Send confirmation code' }</Text>}
        </Button>
        <View>
          <Text style={styles.disclaimerText}>By tapping "Send confirmation code" above, we will send you an SMS to confirm your phone number. Message &amp; data rates may apply.</Text>
        </View>
      </View>
    );
  }
}

const RequestCode = reduxForm({
  form: "requestcodeForm",
  validate, 
  onSubmit: onSubmitActions('REQUEST_CODE')
})(RequestForm);

RequestForm.propTypes = {
  
}

function mapDispatchToProps(dispatch) {
  return {
      submitForm: () => dispatch(submit('requestcodeForm')),
  };
}

export default connect(null, mapDispatchToProps)(RequestCode);
