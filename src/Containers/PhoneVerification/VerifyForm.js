import React, { Component } from "react";
import { Platform, TextInput } from "react-native";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import {
  Item,
  Input,
  Button,
  View,
  Text,
  Spinner,
} from "native-base";
import { Field, reduxForm, submit } from "redux-form";
import { onSubmitActions } from 'redux-form-submit-saga';

import styles from "./styles";
import validate from '../../Utils/validate';
import { Colors} from '../../Themes/';

class VerifyForm extends Component {

  constructor(props) {
    super(props);
    this._tryAgain = this._tryAgain.bind(this);
    this._renderInput = this._renderInput.bind(this);
  }

  _tryAgain = () => {
    this.props.handleTryAgain();
  }

  _renderInput(props) {
    const {input, label, type, meta: { touched, error, warning , submitting }, ...inputProps} = props;
  
    return (
      <TextInput
        {...input}
        {...inputProps}
        disabled={submitting}
        ref={(node) => this.textInput = node}
        type={'TextInput'}
        underlineColorAndroid={'transparent'}
        autoCapitalize={'none'}
        autoCorrect={false}
        placeholder={'_ _ _ _ _ _'}
        keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'numeric'}
        style={[ styles.textInput, styles.textStyle ]}
        returnKeyType='go'
        autoFocus
        placeholderTextColor={Colors.cBlack}
        selectionColor={Colors.cBlack}
        maxLength={6} />
    ); 
  }

  render() {
    const { handleSubmit, error, submitting, submitForm } = this.props;
    return(
      <View ref={'form'} >
        <View style={{ flexDirection: 'row', marginBottom: 20 }}>
          <Field name="twofactor" component={this._renderInput} />
        </View>
        <Button
          large 
          block 
          style={[styles.btn, styles.btnPrimary]}
          onPress={() => submitForm()}>
          {submitting ? <Spinner color="white" size="small"/> : <Text style={styles.btnText}>{ 'Verify confirmation code' }</Text>}
        </Button>
        <View style={{marginTop: 10}}>
          <Text style={styles.wrongNumberText} onPress={this._tryAgain}>
            Enter the wrong number or need a new code?
          </Text>
        </View>
      </View>
    );
  }
}

const VerifyCode = reduxForm({
  form: "verifycodeForm",
  validate, 
  onSubmit: onSubmitActions('VERIFY_CODE')
})(VerifyForm);

VerifyForm.propTypes = {
  
}

function mapDispatchToProps(dispatch) {
  return {
      submitForm: () => dispatch(submit('verifycodeForm')),
  };
}

export default connect(null, mapDispatchToProps)(VerifyCode);