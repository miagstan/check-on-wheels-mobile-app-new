import React, { Component } from 'react';
import {
  Container,
  Content,
  Header,
  Left,
  Right,
  Body,
  Title,
  View,
  Text,
  H2,
  Button,
} from "native-base";
import { bindActionCreators } from 'redux'
import { connect } from "react-redux";
import CountryPicker from 'react-native-country-picker-modal';
import SignupActions from '../../Redux/SignupRedux';

import If from '../../Components/If';
import RequestForm from "./RequestForm";
import VerifyForm from "./VerifyForm";
import styles from "./styles";


class PhoneVerification extends Component {
    
  render(){
    const {enterCode, email, error, tryAgain} = this.props;    
    let headerText = `What's your ${enterCode ? 'verification code' : 'phone number'}?`

    //TODO change hard coded
    const country = {
      cca2: 'FI',
      callingCode: '358'
    }

    return (
      <Container style={styles.container}>
        <Header style={styles.header}>
          <Left />
          <Body>
            <Title style={[styles.headerColor]}>
            Verification
            </Title>
          </Body>
          <Right />
        </Header>
        <Content style={styles.content} showsHorizontalScrollIndicator={false} >
          <H2 style={styles.headerText}>{headerText}</H2>
          <View  style={styles.formContainer}>
            <If condition={enterCode === false}>
              <RequestForm error={error} initialValues={{email, country}} />
            </If>
            <If condition={enterCode !== false}>
              <VerifyForm error={error} initialValues={{email}} handleTryAgain={tryAgain} />
            </If>
          </View>
        </Content>
      </Container>
    )
  }
}


const mapStateToProps = (state) => {
  const { signUp } = state;
  
  return {
    error: signUp.error,
    enterCode: signUp.enterCode,
    code: signUp.code,
    email: signUp.account && signUp.account.email ? signUp.account.email : "",
  }
}

function mapDispatchToProps(dispatch) {
  return {
    tryAgain: bindActionCreators( SignupActions.tryAgain, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhoneVerification)