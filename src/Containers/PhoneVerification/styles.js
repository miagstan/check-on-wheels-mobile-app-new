const React = require("react-native");
const { StyleSheet, Platform } = React;

import { Metrics, Colors, Fonts } from '../../Themes/';
const brandColor = '#744BAC';
export default {
  container: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  content: {
    //margin: 30,
    padding: 30,
    backgroundColor: Colors.themeBg,
  },
  header: {
    backgroundColor: Colors.fireLight,
    borderBottomColor: Colors.cGreyLight,
  },
  headerColor: {
    color: Colors.snow,
  },
  formContainer: {
    //padding: 30,
  },
  headerText: {
    fontFamily: "Mosk-Normal-400",
    color: Colors.cBlack,    
    textAlign: 'center',
    fontSize: 22,
    marginBottom: 30,
  },

  btn: {
    borderRadius: null,
  },
  btnText: {
    fontFamily: "Mosk-Medium-500",
    fontSize: 20,
  },
  btnPrimary: {
    backgroundColor: Colors.fireLight,
  },


  countryPicker: {
    alignItems: 'center',
    justifyContent: 'center'
  },

  header_: {
    textAlign: 'center',
    marginTop: 60,
    fontSize: 22,
    margin: 20,
    color: '#4A4A4A',
  },
  form: {
    margin: 20
  },
  textInput: {
    padding: 0,
    margin: 0,
    flex: 1,
    color: Colors.cBlack, 
    fontSize: 24,
    fontFamily: 'Mosk-Normal-400',
    paddingTop: 4,
  },
  textStyle: {
    height: 50,
    textAlign: 'center',
    fontSize: 40,
    fontFamily: 'Mosk-Bold-700',
    //letterSpacing: '2',
  },
  button: {
    marginTop: 20,
    height: 50,
    backgroundColor: brandColor,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    fontFamily: 'Helvetica',
    fontSize: 16,
    fontWeight: 'bold'
  },
  wrongNumberText: {
    fontFamily: "Mosk-Medium-500",
    color: Colors.cBlack, 
    margin: 10,
    fontSize: 15,
    textAlign: 'center'
  },
  disclaimerText: {
    marginTop: 30,
    fontSize: 12,
    color: 'grey'
  },
  callingCodeView: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  callingCodeText: {
    fontSize: 24,
    color: Colors.fireLight,
    fontFamily: 'Mosk-Normal-400',
    paddingRight: 10,
    paddingTop: 4,
  }
}