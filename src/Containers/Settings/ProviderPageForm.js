import React, { Component } from "react";
import { Platform, UIManager, LayoutAnimation, Keyboard } from "react-native";
import { Container, Content, Header, Left, Right, Title, Body, Icon, Form, Button, View, Text, Spinner } from 'native-base';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { onSubmitActions } from 'redux-form-submit-saga';
import { Field, reduxForm, submit } from 'redux-form';

import FormInput from '../../Components/FormInput';
import FormTextarea from '../../Components/FormTextarea';
import DatePickerInput from '../../Components/DatePickerInput';

import validate from '../../Utils/validate';
import styles from "./styles";

export class AboutForm extends Component {

  constructor(props) {
    super(props);
  }

  render(){
    const { handleSubmit, error, submitting, submitForm, navigation } = this.props;
    
    return (
      <Container style={styles.container}>
        <Header style={styles.header}>
          <Left>
            <Button 
              transparent
              onPress={() => navigation.goBack()}>
              <Icon name='arrow-back' style={[styles.headerColor]} />
            </Button>
          </Left>
          <Body>
            <Title style={[styles.headerColor]}>
              About
            </Title>
          </Body>
          <Right />
        </Header>
      <Content style={styles.content} showsHorizontalScrollIndicator={false}>
        <Form style={styles.formContainer} onSubmit={handleSubmit}>
          <Field name="professionalStatement" label="Professional Statement" component={FormTextarea} returnKeyType='next' autoFocus/>  
          <Field name="practicingStartDate" label="Practicing Start Date" component={DatePickerInput} returnKeyType='next' autoFocus/>  
          <View style={{ marginTop: 30 }}>
            <Button block rounded  style={[styles.btn, styles.btnPrimary]}  onPress={() => submitForm()}>
              {submitting ? <Spinner color="white" size="small"/> : <Text style={styles.btnText}>Save</Text>}
            </Button>
          </View>
        </Form>
      </Content>
      </Container>
    );
  }
}

const AboutScreen = reduxForm({
  form: 'providerForm',
  validate,
  onSubmit: onSubmitActions('PROVIDER_INFO')
})(AboutForm);


const mapStateToProps = (state) => {
  const { auth, accountInfo } = state;
  const { provider: { professionalStatement, practicingStartDate, id } } = accountInfo;
  return {
    user: auth.user,
    initialValues: {
      accountId: auth.userId,
      professionalStatement,
      practicingStartDate,
      id,
    }
  }
}

function mapDispatchToProps(dispatch) {
  return {
      submitForm: () => dispatch(submit('providerForm')),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AboutScreen);