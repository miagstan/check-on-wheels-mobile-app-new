import React from 'react';
import { connect } from 'react-redux';
import { Container, Header, Title, Content, View, List, ListItem, Left, Body, Right, Thumbnail, Text, Button, Icon } from 'native-base';
import { NavigationActions } from 'react-navigation';

import If from '../../Components/If';
import AuthActions from '../../Redux/AuthRedux';

// Styles
import styles from './styles';

const defaultUser = require("../../Images/user_default.png");


class Settings extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      editingQualification: false,
    }

    this._editQualification = this._editQualification.bind(this);
    
  }

  _editQualification(){
    this.setState({
      editingQualification: !this.state.editingQualification
    })
  }

  render () {
    const { user, receiver, provider, navigation, providerFormScreen } = this.props;
    const { goBack } = navigation;
    const { editingQualification } = this.state;
    const qEditText = editingQualification ? 'Done' : 'Edit';

    const dob = user.dateOfBirth ?  new Date(user.dateOfBirth).toLocaleDateString() : ""; 

    var items = ['Simon Mignolet','Nathaniel Clyne','Dejan Lovren'];
    
    return (
      <Container style={styles.container}>
        <Header style={styles.header}>
          <Left />
          <Body>
            <Title style={[styles.headerColor]}>
              Settings
            </Title>
          </Body>
          <Right />
        </Header>
        <Content style={styles.content} showsHorizontalScrollIndicator={false} >
          <List>
            <ListItem first last style={[styles.listItem, { marginTop: 40}]}>
              <Thumbnail  source={defaultUser} />
              <Body>
                <Text style={styles.nameTitle}>{`${user.firstName} ${user.lastName}`}</Text>
                <Text note>Doing what you like will always keep you happy . .</Text>
              </Body>
              <Right>
                <View style={styles.iconBg}>
                  <Icon name="md-create" style={styles.editIcon} />
                </View>
              </Right>
            </ListItem>
            <ListItem itemHeader style={styles.itemHeader}>
              <Text uppercase style={styles.itemHeaderTextColor}>Date of Birth</Text>
            </ListItem>
            <ListItem last style={[styles.listItem]}>
              <Text style={styles.normalTextColor}>{dob}</Text>
            </ListItem>

            <ListItem itemHeader style={styles.itemHeader}>
              <Text uppercase style={styles.itemHeaderTextColor}>Phone Number</Text>
            </ListItem>
            <ListItem last style={[styles.listItem]}>
              <Text style={styles.normalTextColor}>{user.contactNumber}</Text>
            </ListItem>

            <If condition={user.accountType === 'Provider' }>
              <ListItem itemHeader last style={styles.itemHeader}>
                <Left>
                  <Text uppercase style={[styles.itemHeaderTextColor, { marginLeft: null }]}>About</Text>
                </Left>
                <Body />
                <Right>
                  <Text style={styles.itemHeaderEditTextColor} onPress={providerFormScreen}>Edit</Text>
                </Right>
              </ListItem>
            </If>
            <If condition={user.accountType === 'Provider' }>
              <ListItem last style={[styles.listItem]}>
                <Text style={styles.normalTextColor}>{provider.professionalStatement}</Text>
              </ListItem>
            </If>

            <ListItem itemHeader style={styles.itemHeader}>
              <Left>
                <Text uppercase style={[styles.itemHeaderTextColor, { marginLeft: null }]}>Qualifications</Text>
              </Left>
              <Body />
              <Right>
                <Text style={styles.itemHeaderEditTextColor} onPress={this._editQualification}>{qEditText}</Text>
              </Right>
            </ListItem>
            { items.map((item, index) => (
              <ListItem key={index} last style={[styles.listItem]}>
                <View style={styles.qualificationView}>
                  <If condition={editingQualification}>
                    <Icon name="ios-remove-circle" style={styles.removeIcon}/>
                  </If>
                  <Text style={[styles.normalTextColor]}>{item}</Text>
                </View>
                <If condition={editingQualification}>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </If>
              </ListItem>
            ))}
            <If condition={editingQualification}>
              <ListItem last style={[styles.listItem]}>
                <View style={styles.qualificationView}>
                  <Icon name="ios-add-circle" style={styles.addIcon}/>
                  <Text style={[styles.normalTextColor]}>add qualification</Text>
                </View>
              </ListItem>
            </If>

            <ListItem itemHeader style={styles.itemHeader} />
            <ListItem last style={[styles.listItem, styles.logOutListItem]}>
              <Text 
                style={styles.logOutText} 
                onPress={this.props.logout}>
                Log Out
              </Text>
            </ListItem>
          </List>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  const { auth, accountInfo } = state;
  const { user } = auth;
  const { provider, receiver } = accountInfo;
  
  return {
    user,
    provider,
    receiver,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(AuthActions.logout()),
    providerFormScreen: () => dispatch(NavigationActions.navigate({ routeName: 'ProviderFormScreen' })),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings)