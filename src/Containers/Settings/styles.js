import {Dimensions, Platform} from 'react-native'

import { Metrics, Colors, Fonts } from '../../Themes/';

export default {
  container: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  content: {
    backgroundColor: Colors.themeBg,
  },
  header: {
    backgroundColor: Colors.fireLight,
    borderBottomColor: Colors.cGreyLight,
  },
  headerColor: {
    color: Colors.snow,
  },
  headerText: {
    fontFamily: "Mosk-Normal-400",
    color: Colors.cBlack,    
    textAlign: 'center',
    fontSize: 22,
    marginBottom: 30,
  },
  formContainer: {
    padding: 30,
  },
  listItem: {
    backgroundColor: Colors.snow,
  },
  itemHeader: {
    borderBottomWidth: null,
  },
  normalTextColor: {
    color: Colors.cBlack,
  },
  itemHeaderTextColor: {
    color: Colors.cGrey,
  },
  logOutListItem: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logOutText: {
    color: Colors.fire,
    fontSize: 18,
  },
  nameTitle: {
    fontSize: 24,
    fontFamily: "Mosk-Medium-500",
    color: Colors.cBlack,
  },
  iconBg: {
    backgroundColor: Colors.themeBg,
    width: 40,
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editIcon: {
    fontSize: 25,
    color: Colors.fireLight,
    backgroundColor: 'transparent',
  },
  itemHeaderEditTextColor: {
    color: Colors.fireLight,
    fontSize: 18,
  },
  qualificationView: {
    flex: 1, 
    flexDirection: 'row',
    alignItems: 'center',
  },
  removeIcon: {
    fontSize: 25,
    color: Colors.fire,
    marginRight: 15
  },
  addIcon: {
    fontSize: 25,
    color: Colors.cGreen,
    marginRight: 15
  },
  btn: {
    borderRadius: null,
  },
  btnText: {
    fontFamily: "Mosk-Medium-500",
    fontSize: 20,
  },
  btnPrimary: {
    backgroundColor: Colors.fireLight,
  },
}
