import React, { Component } from "react";
import { Platform, UIManager, LayoutAnimation, Keyboard } from "react-native";
import { Form, Button, View, Text, Spinner } from 'native-base';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { onSubmitActions } from 'redux-form-submit-saga';
import { Field, reduxForm, submit } from 'redux-form';

import FormInput from '../../Components/FormInput';
import validate from '../../Utils/validate';
import styles from "./styles";

export class SignUpForm extends Component {

  constructor(props) {
    super(props);
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true)
    }
  }

  componentDidMount() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring)
  }

  render(){
    const { handleSubmit, error, submitting, submitForm } = this.props;
    
    return (
      <Form style={styles.shadow} onSubmit={handleSubmit}>
        <Field name="firstName" label="First Name" component={FormInput} icon="person" returnKeyType='next'/>
        <Field name="lastName" label="Last Name" component={FormInput} icon="person" returnKeyType='next'/>
        <Field name="email" label="Email" component={FormInput} icon="ios-mail-open-outline" keyboardType="email-address" returnKeyType='next'/>
        <Field name="password" label="Password" component={FormInput} icon="unlock" secureTextEntry returnKeyType='next'/>
        <Field name="confirmPassword" label="Re-Type Password" component={FormInput} icon="unlock" secureTextEntry returnKeyType='send' onSubmitEditing={() => {submitForm(); return Keyboard.dismiss}}/>
  
        <View style={{ marginTop: 30 }}>
          <Text style={styles.termsInfo}>Choosing Next means that you agree to the Privacy Statement.</Text>
          <View style={{ marginTop: 40 }} />
          <Button block rounded  style={[styles.btn, styles.btnPrimary]}  onPress={() => submitForm()}>
            {submitting ? <Spinner color="white" size="small"/> : <Text style={styles.btnText}>Save and continue</Text>}
          </Button>
        </View>
      </Form>
    );
  }
}

const SignUpScreen = reduxForm({
  form: 'signupForm',
  validate,
  onSubmit: onSubmitActions('SIGN_UP')
})(SignUpForm);

function mapDispatchToProps(dispatch) {
  return {
      submitForm: () => dispatch(submit('signupForm')),
  };
}

export default connect(null, mapDispatchToProps)(SignUpScreen);