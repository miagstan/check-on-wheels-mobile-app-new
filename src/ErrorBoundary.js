import React from 'react';
import {View, Text, H3} from 'native-base';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: null, errorInfo: null };
  }
  
  componentDidCatch(error, errorInfo) {
    // Catch errors in any components below and re-render with error message
    this.setState({
      error: error,
      errorInfo: errorInfo
    })
    // You can also log error messages to an error reporting service here
  }
  
  render() {
    if (this.state.errorInfo) {
      // Error path
      return (
        <View>
          <H3>Something went wrong.</H3>
          <View>
            {this.state.error && <Text>this.state.error.toString()</Text>}
            <Text>{this.state.errorInfo.componentStack}</Text>
          </View>
        </View>
      );
    }
    // Normally, just render children
    return this.props.children;
  }  
}

export default ErrorBoundary;