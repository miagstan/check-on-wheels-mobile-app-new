import { Platform } from 'react-native';
import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { StackNavigator, addNavigationHelpers } from 'react-navigation';
import LoadingScreen from '../Containers/LoadingScreen';
import LoggedInStackNavigator from './LoggedInStackNavigator';
import NotLoggedInStackNavigator from './NotLoggedInStackNavigator';
import ProviderPageForm from "../Containers/Settings/ProviderPageForm";

import styles from './Styles/NavigationStyles';

// Manifest of possible screens
export const PrimaryNav = StackNavigator({
  LoadingScreen: { screen: LoadingScreen },
  LoggedInStack: { screen: LoggedInStackNavigator },
  NotLoggedInStack: { screen: NotLoggedInStackNavigator },  
  ProviderFormScreen: { screen: ProviderPageForm },
}, {
  // Default config for all screens
  headerMode: 'none',
  /*
  * Use modal on iOS because the card mode comes from the right,
  * which conflicts with the drawer example gesture
  */
  mode: Platform.OS === 'ios' ? /* 'modal' */ 'card' : 'card',
  navigationOptions: {
    headerStyle: styles.header
  }
})

const Navigation = ({ dispatch, navigation }) => {
  return (
    <PrimaryNav navigation={addNavigationHelpers({ dispatch, state: navigation })} />
  )
}

Navigation.propTypes = {
  dispatch: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired
}

function mapStateToProps (state) {
  return {
    navigation: state.navigation
  }
}

// export default PrimaryNav
export default connect(mapStateToProps)(Navigation)