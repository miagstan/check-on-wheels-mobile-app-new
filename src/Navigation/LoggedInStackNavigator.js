import React from "react";


import { TabNavigator, StackNavigator } from 'react-navigation';
import { FontAwesome } from "react-native-vector-icons";
import { Platform, StatusBar} from 'react-native'
import { Icon } from 'native-base';

import Home from "../Containers/Home";
import Settings from "../Containers/Settings";

import { Colors } from '../Themes/';

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
export default TabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarLabel: "Home",
        tabBarIcon: ({ tintColor }) =>
          <FontAwesome name="home" size={30} color={Colors.fireLight} />
      }
    },
    Settings: {
      screen: Settings,
      navigationOptions: {
        tabBarLabel: "Settings",
        tabBarIcon: ({ tintColor }) =>
          <FontAwesome name="gear" size={30} color={Colors.fireLight} />
      }
    }
  },
  {
    tabBarOptions: {
      style: {
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
      }
    },
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
  }
);