import { StackNavigator } from 'react-navigation';

import NotLoggedIn from "../Containers/NotLoggedIn";
import SignUp from "../Containers/SignUp";
import SignIn from "../Containers/SignIn";
import PhoneVerification from "../Containers/PhoneVerification";

export default StackNavigator(
  {
    Initial: {
      screen: NotLoggedIn,
    },
    SignUp: {
      screen: SignUp,
    },
    SignIn: {
      screen: SignIn,
    },
    PhoneVerification: {
      screen: PhoneVerification,
    },
  },
  {
    initialRouteName: "Initial",
    headerMode: "none",
  }
);