import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

import { mergeIn } from '../Utils/helpers';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  providerInfoSubmit: null,
  providerInfoSuccess: ['payload'],
  providerInfoFailure: ['payload'],
})

export const AccountInfoTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  receiver: {
    id: false,
  },
  provider: {
    id: false,    
    professionalStatement: null,
    practicingStartDate: null,
    accountId: null,
  },
  error: null,
})

/* ------------- Reducers ------------- */

// providerInfo success
export const providerInfoSuccess = (state, action ) => {
  return  mergeIn(state, ['provider'], action.payload, { deep: true });
}

export const providerInfoFailure = (state, action) =>
state.merge({ error: action.error })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PROVIDER_INFO_SUCCESS]: providerInfoSuccess
})

/* ------------- Selectors ------------- */

// Is rehydration complete?
export const isRehydrationComplete = (state) => state.rehydrationComplete