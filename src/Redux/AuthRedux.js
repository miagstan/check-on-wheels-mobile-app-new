import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  loginSubmit: null,
  loginSuccess: ['payload', 'isNew'],
  loginFailure: ['payload'],
  logout: null,
  autoLogin: null,
  registrationIncomplete: null,
  incompleteProfile: null,
})

export const AuthTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  userId: false,
  user: false,
  error: null,
  fetching: false,
})

/* ------------- Reducers ------------- */

// we're attempting to login
export const request = (state) => state.merge({ fetching: true, error: null })

// we've successfully logged in
export const success = (state, action) => {
  const { user, userId } = action.payload;
  return  state.merge({ fetching: false, error: null, userId,  user})
}

// we've had a problem logging in
export const failure = (state, { payload }) =>
  state.merge({ fetching: false, error: payload.error })

// we've logged out
export const logout = (state) => INITIAL_STATE

// startup saga invoked autoLogin
export const autoLogin = (state) => state

// startup saga invoked autoLogin
export const registrationIncomplete = (state) => state

// startup saga invoked autoLogin
export const incompleteProfile = (state) => state

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_SUBMIT]: request,
  [Types.LOGIN_SUCCESS]: success,
  [Types.LOGIN_FAILURE]: failure,
  [Types.LOGOUT]: logout,
  [Types.AUTO_LOGIN]: autoLogin,
  [Types.REGISTRATION_INCOMPLETE]: registrationIncomplete,
  [Types.INCOMPLETE_PROFILE]: incompleteProfile,
})

/* ------------- Selectors ------------- */

// Is the current user logged in?
export const isLoggedIn = (loginState) => loginState.user !== false;