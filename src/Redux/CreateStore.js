import { createStore, applyMiddleware, compose } from 'redux';
import { autoRehydrate } from 'redux-persist';
import { createLogger } from 'redux-logger';
import Immutable from 'seamless-immutable';

import Config from '../Config/DebugConfig';
import createSagaMiddleware from 'redux-saga';
import RehydrationServices from '../Services/RehydrationServices';
import ReduxPersist from '../Config/ReduxPersist';

const isDebuggingInChrome = __DEV__ && !!window.navigator.userAgent;

const stateTransformer = (state) => {
    let newState = {}
    for (var i of Object.keys(state)) {
        if (Immutable.isImmutable(state[i])) {
            newState[i] = state[i].asMutable({deep: true});
        } else {
            newState[i] = state[i];
        }
    };
    return newState;
};

const logger = createLogger({
    predicate: (getState, action) => isDebuggingInChrome,
    collapsed: true,
    duration: true,
    diff: true,
    stateTransformer
});

// creates the store
export default (rootReducer, rootSaga) => {
  /* ------------- Redux Configuration ------------- */

  const middleware = []
  const enhancers = []

  /* ------------- Saga Middleware ------------- */

  const sagaMonitor = __DEV__ ? console.tron.createSagaMonitor() : null
  //const sagaMonitor = null
  
  const sagaMiddleware = createSagaMiddleware({ sagaMonitor })
  middleware.push(sagaMiddleware)

  /* ------------- Add redux logger ------------- */
  middleware.push(logger);
  
  /* ------------- Assemble Middleware ------------- */

  enhancers.push(applyMiddleware(...middleware))

  /* ------------- AutoRehydrate Enhancer ------------- */

  // add the autoRehydrate enhancer
  if (ReduxPersist.active) {
    enhancers.push(autoRehydrate())
  }

  // if Reactotron is enabled (default for __DEV__), we'll create the store through Reactotron
  const createAppropriateStore = Config.useReactotron ? console.tron.createStore : createStore
  const store = createAppropriateStore(rootReducer, compose(...enhancers))

  // configure persistStore and check reducer version number
  if (ReduxPersist.active) {
    RehydrationServices.updateReducers(store)
  }

  // kick off root saga
  sagaMiddleware
    .run(rootSaga)
    .done.catch(err => console.log('[SAGA-ERROR]', err));

  return store
}
