import { NavigationActions } from 'react-navigation'
import { PrimaryNav } from '../Navigation/AppNavigation'

const { navigate, reset } = NavigationActions;
const { getStateForAction } = PrimaryNav.router;

const INITIAL_STATE = getStateForAction(
  navigate({ routeName: 'LoadingScreen' })
)
const NOT_LOGGED_IN_STATE = getStateForAction(reset({
  index: 0,
  actions: [
    navigate({ routeName: 'NotLoggedInStack' })
  ]
}))

const IN_COMPLETE_REGISTRATION_STATE = getStateForAction(reset({
  index: 0,
  actions: [
    navigate({ routeName: 'NotLoggedInStack' })
  ]
}))

const LOGGED_IN_STATE = getStateForAction(reset({
  index: 0,
  actions: [
    navigate({ 
      routeName: 'LoggedInStack'
    })
  ]
}))

const INCOMPLETE_PROFILE_STATE = getStateForAction(reset({
  index: 0,
  actions: [
    navigate({ 
      routeName: 'LoggedInStack',
      action: navigate({
        routeName: 'Settings'
      })
    })
  ]
}))


/**
 * Creates an navigation action for dispatching to Redux.
 *
 * @param {string} routeName The name of the route to go to.
 */
// const navigateTo = routeName => () => navigate({ routeName })

export function reducer (state = INITIAL_STATE, action) {
  let nextState
  switch (action.type) {
    case 'SET_REHYDRATION_COMPLETE':
      return NOT_LOGGED_IN_STATE
    case 'REGISTRATION_INCOMPLETE':
      return IN_COMPLETE_REGISTRATION_STATE
    case 'LOGOUT':
      return NOT_LOGGED_IN_STATE
    case 'LOGIN_SUCCESS':
    case 'PROVIDER_INFO_SUCCESS':
      return action.isNew ? INCOMPLETE_PROFILE_STATE : LOGGED_IN_STATE
    case 'AUTO_LOGIN':
      return INCOMPLETE_PROFILE_STATE //LOGGED_IN_STATE
  }
  nextState = getStateForAction(action, state)
  return nextState || state
}