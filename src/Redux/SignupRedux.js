import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  tryAgain: null,
  signUpSubmit: null,
  signUpSuccess: ['account'],
  signUpFailure: ['payload'],
  requestCodeSubmit: null,
  requestCodeSuccess: ['data'],
  requestCodeFailure: ['payload'],
  verifyCodeSubmit: null,
  verifyCodeSuccess: null,
  verifyCodeFailure: ['payload'],
})

export const SignupTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  account: null,
  error: null,
  enterCode: false,
  code: false,
})

export const signupSuccess = (state, action) => {
  const { account } = action
  return state.merge({ error: null, account })
}

export const signupfailure = (state, action) =>
state.merge({ error: action.error, account: null })

export const tryAgain = (state) => {
  return state.merge({ error: null, enterCode: false, code: false, })
}


export const requestCodeSuccess = (state, action) => {
  const { data } = action
  return state.merge({ error: null, enterCode: true, code: data.timestamp, })
}

export const requestCodeFailure = (state, action) => {
  const { payload } = action
  return state.merge({ error: payload, code: false })
}

export const verifyCodeSuccess = (state) => INITIAL_STATE;

export const verifyCodeFailure = (state, action) => {
  const { payload } = action;
  return state.merge({ error: payload, code: false })
}


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SIGN_UP_SUCCESS]: signupSuccess,
  [Types.REQUEST_CODE_SUCCESS]: requestCodeSuccess,
  [Types.REQUEST_CODE_FAILURE]: requestCodeFailure,
  [Types.VERIFY_CODE_SUCCESS]: verifyCodeSuccess,
  [Types.VERIFY_CODE_FAILURE]: verifyCodeFailure,
  [Types.TRY_AGAIN]: tryAgain,
})

/* ------------- Selectors ------------- */

// Is the user registration in progress?
export const isRegistrationInProgress = (sigUpState) => sigUpState.account !== null;

// get comfirmPassword
export const getConfirmPassword = (signUpState) => signUpState.getIn(['account', 'confirmPassword']);