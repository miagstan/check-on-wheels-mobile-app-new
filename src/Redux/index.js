import { combineReducers } from 'redux';
import appSaga from '../Sagas/';
import { addFormSubmitSagaTo } from 'redux-form-submit-saga';
import { reducer as form } from 'redux-form';

import configureStore from './CreateStore';

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    navigation: require('./NavigationRedux').reducer,
    appState: require('./AppStateRedux').reducer,
    auth: require('./AuthRedux').reducer,
    signUp: require('./SignupRedux').reducer,
    accountInfo: require('./AccountInfoRedux').reducer,
    form,
  })
  const rootSaga = addFormSubmitSagaTo(appSaga);
  return configureStore(rootReducer, rootSaga);
}
