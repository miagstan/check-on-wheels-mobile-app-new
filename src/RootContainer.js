import React, { Component } from 'react'
import { Root } from "native-base";
import Navigation from './Navigation/AppNavigation'
import { connect } from 'react-redux'
import StartupActions from './Redux/StartupRedux'
import ReduxPersist from './Config/ReduxPersist'

// Styles
import styles from './styles'

class RootContainer extends Component {
  componentDidMount () {
    // if redux persist is not active fire startup action
    if (!ReduxPersist.active) {
      this.props.startup()
    }
  }

  render () {
    return (
      <Root style={styles.root}>
        <Navigation />
      </Root>
    )
  }
}

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = (dispatch) => ({
  startup: () => dispatch(StartupActions.startup())
})

export default connect(null, mapDispatchToProps)(RootContainer)