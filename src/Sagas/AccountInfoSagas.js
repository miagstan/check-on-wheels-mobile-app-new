import { put, call, select } from 'redux-saga/effects';
import AccountInfoActions from '../Redux/AccountInfoRedux';

// update info
export const updateproviderinfo = function * updateproviderinfo (api, action) {
  
  let payload = action.payload;
  if(!payload.id) delete payload.id;

  const response = yield call(api.updateProviderInfo, payload);

  if (response.ok) {
    const provider = response.data;
    yield put(AccountInfoActions.providerInfoSuccess(provider));
  } else {
    yield put(AccountInfoActions.providerInfoFailure(response.error))
  }

}
