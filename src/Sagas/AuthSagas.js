import { put, call } from 'redux-saga/effects';
import AuthActions from '../Redux/AuthRedux';
import { setAuthToken, getAuthToken, deleteAuthToken } from '../Services/SecuredStorageService';


// attempts to login
export const login = function * login (api, { payload }) {
  const response = yield call(api.login, payload);
  if (response.ok) {
    yield call(setAuthToken, response.data);
    yield put(AuthActions.loginSuccess(response.data));
  } else {
    yield put(AuthActions.loginFailure(response.error))
  }
}

export const logout = function * logout (api) {
  yield call(api.logout);
  yield call(deleteAuthToken);
}

