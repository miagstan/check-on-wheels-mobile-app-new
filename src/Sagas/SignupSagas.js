import { put, call, select } from 'redux-saga/effects';
import { NavigationActions } from 'react-navigation';

import SignupActions, { getConfirmPassword } from '../Redux/SignupRedux';
import { setAuthToken } from '../Services/SecuredStorageService';
import AuthActions from '../Redux/AuthRedux'

export const selectConfirmPassword = (state) => getConfirmPassword(state.signUp)

// attempts to login
export const signup = function * signup (api, action) {

  let payload = action.payload;
  payload.email = payload.email.toLowerCase();

  const response = yield call(api.postAccount, payload);

  if (response.ok) {
    const account = response.data;
    //delete account.confirmPassword;
    yield put(SignupActions.signUpSuccess(account));
    yield put(NavigationActions.navigate({
      routeName: 'PhoneVerification'
    }));
  } else {
    yield put(SignupActions.signUpFailure(response.error))
  }

}

export const requestcode = function * requestcode (api, action) {
  const { payload } = action;
  const { country, email } = payload;
  const contactNumber = `+${country.callingCode}${payload.contactNumber}`;
  const password = yield select(selectConfirmPassword)
  const transformedPayload = {
    email, 
    password, 
    contactNumber
  };
  
  const response = yield call(api.requestCode, transformedPayload);

  if (response.ok) {
    yield put(SignupActions.requestCodeSuccess(response.data));
  } else {
    yield put(SignupActions.requestCodeFailure(response.error))
  }
}

export const verifycode = function * verifycode (api, action) {
  const { payload } = action;
  const response = yield call(api.loginWithCode, payload);
  if (response.ok) {
    yield call(setAuthToken, response.data);
    yield put(SignupActions.verifyCodeSuccess());
    yield put(AuthActions.loginSuccess(response.data, true));
  } else {
    yield put(SignupActions.verifyCodeFailure(response.error))
  }
}