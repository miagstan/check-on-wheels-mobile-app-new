import { put, select, call } from 'redux-saga/effects'
import AppStateActions from '../Redux/AppStateRedux'
import { is } from 'ramda'
import AuthActions, { isLoggedIn } from '../Redux/AuthRedux'
import { isRegistrationInProgress } from '../Redux/SignupRedux'
import { getAuthToken } from '../Services/SecuredStorageService';

// exported to make available for tests
export const selectLoggedInStatus = (state) => isLoggedIn(state.auth)
export const selectRegistrationInProgressStatus = (state) => isRegistrationInProgress(state.signUp)


// process STARTUP actions
const startup = function * startup (action) {
  yield put(AppStateActions.setRehydrationComplete())
  //const isLoggedIn = yield select(selectLoggedInStatus)
  const token = yield call(getAuthToken);
  if (token) {
    yield put(AuthActions.autoLogin())
  } else {
    const isRegistrationInProgress = yield(selectRegistrationInProgressStatus)    
    if(isRegistrationInProgress){
      yield put(AuthActions.registrationIncomplete())
    }
  }
}


export default startup;