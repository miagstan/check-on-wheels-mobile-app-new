import { all, takeLatest } from 'redux-saga/effects'
import API from '../Services/Api';

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux';
import { AuthTypes } from '../Redux/AuthRedux';
import { SignupTypes } from '../Redux/SignupRedux'
import { AccountInfoTypes } from '../Redux/AccountInfoRedux';


/* ------------- Sagas ------------- */

import startup from './StartupSagas';
import { login, logout } from './AuthSagas';
import { signup, requestcode, verifycode } from './SignupSagas';
import { updateproviderinfo } from './AccountInfoSagas';

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.create();

/*
 * The entry point for all the sagas used in this application.
 */
const root = function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(AuthTypes.LOGIN_SUBMIT, login, api),

    // some sagas receive extra parameters in addition to an action
    takeLatest(SignupTypes.SIGN_UP_SUBMIT, signup, api),
    takeLatest(SignupTypes.REQUEST_CODE_SUBMIT, requestcode, api),
    takeLatest(SignupTypes.VERIFY_CODE_SUBMIT, verifycode, api),

    takeLatest(AuthTypes.LOGOUT, logout, api),
    
    takeLatest(AccountInfoTypes.PROVIDER_INFO_SUBMIT, updateproviderinfo, api)
    
  ])
}
export default root