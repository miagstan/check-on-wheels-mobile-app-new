// a library to wrap and simplify api calls
import apisauce from 'apisauce';
import { getAuthToken } from './SecuredStorageService';

const formatError = (message, errorArr) => {
  let errors = errorArr;
  errors['_error'] = message
  return errors;
}

// our "constructor"
const create = (baseURL = 'https://check-on-wheels.herokuapp.com/api') => {
  // ------
  // STEP 1
  // ------
  //
  // Create and configure an apisauce-based api object.
  //
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache'
    },
    // 10 second timeout...
    timeout: 10000
  })

  api.addResponseTransform(response => {
    if (!response.ok) {
      const { message, details = {} } = response.data.error;
      if (response.status == 422) {
        //let errors = details.messages;
        //errors['_error'] = message;
        response.error = details.messages;
      } else {
        response.error = { error: message };
      }
    }
  });

  api.addAsyncRequestTransform(request => async () => {
    const authToken = await getAuthToken();
    if(authToken) {
      request.headers['Authorization'] = authToken
    }
  });

  // Wrap api's addMonitor to allow the calling code to attach
  // additional monitors in the future.  But only in __DEV__ and only
  // if we've attached Reactotron to console (it isn't during unit tests).
  if (__DEV__ && console.tron) {
    api.addMonitor(console.tron.apisauce)
  }

  // ------
  // STEP 2
  // ------
  //
  // Define some functions that call the api.  The goal is to provide
  // a thin wrapper of the api layer providing nicer feeling functions
  // rather than "get", "post" and friends.
  //
  // I generally don't like wrapping the output at this level because
  // sometimes specific actions need to be take on `403` or `401`, etc.
  //
  // Since we can't hide from that, we embrace it by getting out of the
  // way at this level.
  //

  const postAccount = (account) => api.post('/accounts', account );
  const requestCode = (credentials) => api.post('/accounts/requestCode', credentials);
  const loginWithCode = (credentials) => api.post('/accounts/loginWithCode', credentials);
  const login = (credentials) => api.post('/accounts/login?include=user', credentials, {});
  const logout = (token) => api.post('/accounts/logout');
  const updateProviderInfo = (provider) => api.put('/providers', provider);
  
  // ------
  // STEP 3
  // ------
  //
  // Return back a collection of functions that we would consider our
  // interface.  Most of the time it'll be just the list of all the
  // methods in step 2.
  //
  // Notice we're not returning back the `api` created in step 1?  That's
  // because it is scoped privately.  This is one way to create truly
  // private scoped goodies in JavaScript.
  //
  return {
    // a list of the API functions from step 2
    postAccount,
    requestCode,
    loginWithCode,
    login,
    logout,
    updateProviderInfo,
  }
}

// let's return back our create method as the default.
export default {
  create
}
