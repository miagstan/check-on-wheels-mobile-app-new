import { SecureStore } from 'expo';


const AUTH_KEY = 'wheels-auth-key';

export function setAuthToken({id, ttl}) {
  try {
    SecureStore.setItemAsync(id, AUTH_KEY, {});
  } catch (error) {
    console.log('Error saving to SecureStore:', error)
  }
};

export async function getAuthToken(){
  try {
    const token = await SecureStore.getItemAsync(AUTH_KEY);
    return token;
  } catch (error) {
    //console.log('Error fetching from SecureStore:', error)
  }
};

export function deleteAuthToken(){
  try {
    SecureStore.deleteItemAsync(AUTH_KEY)
  } catch (error) {
    console.log('Error deleting from SecureStore:', error)
  }
};