import Immutable from 'seamless-immutable';

export const mergeIn = (target, path, source, config) => {
  let innerTarget = Immutable.getIn(target, path);
  let innerMerged = Immutable.merge(innerTarget, source, config);
  return Immutable.setIn(target, path, innerMerged);
}