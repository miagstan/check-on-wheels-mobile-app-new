const validate = values => {
  const error = {};
  error.email = "";
  error.password = "";
  error.firstName = "";
  error.lastName = "";
  error.contactNumber = "";
  error.professionalStatement = "";
  error.practicingStartDate = "";

  var ema = values.email;
  var pw = values.password;
  var fn = values.firstName;
  var ln = values.lastName;
  var cn = values.contactNumber;
  var ps = values.professionalStatement;
  var psd = values.practicingStartDate;

  if(values.firstName === undefined) {
    fn = "";
  }

  if(values.lastName === undefined) {
    ln = "";
  }

  if(values.contactNumber === undefined) {
    cn = "";
  }
  
  if (values.email === undefined) {
    ema = "";
  }
  if (values.password === undefined) {
    pw = "";
  }

  if (fn === "") {
    error.firstName = "Required";
  }

  if (ln === "") {
    error.lastName = "Required";
  }
  if (cn === "") {
    error.contactNumber = "Required";
  }

  if(ema === "") {
    error.email = "Required";
  }

  if (ema.length < 8 && ema !== "") {
    error.email = "too short";
  }
  if (!ema.includes("@") && ema !== "") {
    error.email = "@ not included";
  }

  if(pw === "") {
    error.password = "Required";
  }

  if (pw.length > 12) {
    error.password = "max 11 characters";
  }
  if (pw.length < 5 && pw.length > 0) {
    error.password = "Weak";
  }

  if (ps === "") {
    error.professionalStatement = "Required";
  }
  if (psd === "") {
    error.practicingStartDate = "Required";
  }

  if (!values.confirmPassword) {
    error.confirmPassword = 'Required';
  }

  if (values.password !== values.confirmPassword) {
    error.confirmPassword = 'Passwords must match';
  }

  return error;
};

export default validate;