import React, { Component } from 'react';
import {
  Container,
  Content,
  Header,
  Left,
  Right,
  Body,
  Title,
  View,
  Text,
  H2,
  Button,
} from "native-base";
import { bindActionCreators } from 'redux'
import { connect } from "react-redux";
import SignupActions from '../../Redux/SignupRedux';

import If from '../../Components/If';

import styles from "./styles";


class Home extends Component {
    
  render(){
    return (
      <Container style={styles.container}>
        <Header style={styles.header}>
          <Left />
          <Body>
            <Title style={[styles.headerColor]}>
              Home
            </Title>
          </Body>
          <Right />
        </Header>
        <Content style={styles.content} showsHorizontalScrollIndicator={false} >
         
        </Content>
      </Container>
    )
  }
}


const mapStateToProps = (state) => {
  const { } = state;
  
  return {
   
  }
}

function mapDispatchToProps(dispatch) {
  return {
    tryAgain: bindActionCreators( SignupActions.tryAgain, dispatch),
  }
}

export default connect(null, mapDispatchToProps)(Home)