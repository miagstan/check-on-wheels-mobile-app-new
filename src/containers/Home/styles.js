import { Dimensions, Platform } from 'react-native'

import { Metrics, Colors, Fonts } from '../../Themes/';

export default {
  container: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  content: {
    padding: 30,
    backgroundColor: Colors.themeBg,
  },
  header: {
    backgroundColor: Colors.fireLight,
    borderBottomColor: Colors.cGreyLight,
  },
  headerColor: {
    color: Colors.snow,
  },
}
