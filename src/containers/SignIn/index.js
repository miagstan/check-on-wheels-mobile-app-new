import React, { Component } from "react";
import {
  Container,
  Content,
  Header,
  Left,
  Right,
  Body,
  Title,
  Button,
  Icon,
  Text,
  View,
  H2,
  Form,
  Spinner 
} from "native-base";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { onSubmitActions } from 'redux-form-submit-saga';
import { Field, reduxForm, submit } from 'redux-form';

import FormInput from '../../Components/FormInput';
import validate from '../../Utils/validate';
import styles from "./styles";
import { Colors } from '../../Themes/';

class SignInForm extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { handleSubmit, fetching, loginError, submitting, submitForm, navigation } = this.props;

    return (
      <Container style={styles.container}>
        <Header style={styles.header}>
          <Left>
            <Button 
              transparent
              onPress={() => navigation.goBack()}>
              <Icon name='arrow-back' style={[styles.headerColor]} />
            </Button>
          </Left>
          <Body>
            <Title style={[styles.headerColor]}>
              Sign In
            </Title>
          </Body>
          <Right />
        </Header>
        <Content style={styles.content} showsHorizontalScrollIndicator={false}>
          <View  style={styles.formContainer}>
            <Form style={styles.shadow} onSubmit={handleSubmit}>
              <Field name="email" label="Email" component={FormInput} icon="ios-mail-open-outline" keyboardType="email-address" returnKeyType='next'/>
              <Field name="password" label="Password" component={FormInput} icon="unlock" secureTextEntry returnKeyType='next'/>
              <View style={{ marginTop: 25 }}>
                <Button block rounded  style={[styles.btn, styles.btnPrimary]}  onPress={() => submitForm()}>
                  {submitting ? <Spinner color="white" size="small"/> : <Text style={styles.btnText}>Sign In</Text>}
                </Button>
              </View>
              { loginError !== null && <Text style={styles.errorInfo} uppercase>{loginError}</Text> }
            </Form>
          </View>
        </Content>
      </Container>
    )
  }
}

const SignInScreen = reduxForm({
  form: 'loginForm',
  validate,
  onSubmit: onSubmitActions('LOGIN')
})(SignInForm);

const mapStateToProps = ({auth}) => {
  return {
    fetching: auth.fetching,
    loginError: auth.error,
    initialValues: {
      "email": "owusuk2013@gmail.com",
      "password": "password"
    }
  }
}

function mapDispatchToProps(dispatch) {
  return {
      submitForm: () => dispatch(submit('loginForm')),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen);
