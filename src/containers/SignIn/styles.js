const React = require("react-native");
const { StyleSheet, Platform } = React;

import { Metrics, Colors, Fonts } from '../../Themes/';

export default {
  container: {
    backgroundColor: "#FFF",
  },
  content: {
    backgroundColor: Colors.themeBg,
  },
  header: {
    backgroundColor: Colors.fireLight,
    borderBottomColor: Colors.cGreyLight,
  },
  btn: {
    borderRadius: null,
    marginBottom: 20,
  },
  btnText: {
    fontFamily: "Mosk-Medium-500",
    fontSize: 20,
  },
  btnPrimary: {
    backgroundColor: Colors.fireLight,
  },
  btnSecondary: {
    backgroundColor: Colors.snow,
  },
  btnSecondaryText: {
    color: Colors.fireLight,
  },
  headerColor: {
    color: Colors.snow,
  },
  textFont: {
    fontFamily: "Mosk-Medium-500",
  },
  formContainer: {
    marginTop: Metrics.screenHeight / 4,
    padding: 30,
  },
  errorInfo: {
    marginTop: 10,
    color: '#ed2f2f',
    textAlign: 'center'
  }
};