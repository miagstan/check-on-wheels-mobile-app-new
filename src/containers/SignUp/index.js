import React, { Component } from "react";
import {
  Container,
  Content,
  Header,
  Left,
  Right,
  Body,
  Title,
  Button,
  Icon,
  Text,
  View,
  H2,
} from "native-base";

import If from '../../Components/If';
import SignupForm from "./form";

import styles from "./styles";
import { Colors } from '../../Themes/';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accountType: false
    }
    this.setAccountType = this.setAccountType.bind(this);
    this._goBack = this._goBack.bind(this);
  }

  setAccountType(type) {
    this.setState({accountType: type})
  }

  _goBack() {
    const { navigation } = this.props;
    const {accountType} = this.state;
    if (accountType) {
      this.setState({accountType: false});
    } else {
      navigation.goBack();
    }
  }

  render() {
    const { navigation } = this.props;
    const { accountType } = this.state;
    return (
      <Container style={styles.container}>
        <Header style={styles.header}>
          <Left>
            <Button 
              transparent
              onPress={() => this._goBack()}>
              <Icon name='arrow-back' style={[styles.headerColor]} />
            </Button>
          </Left>
          <Body>
            <Title style={[styles.headerColor]}>
              Sign Up
            </Title>
          </Body>
          <Right />
        </Header>
        <Content style={styles.content} showsHorizontalScrollIndicator={false}>
          <If condition={accountType === false}>
            <View style={styles.buttonContainer}>
              <H2 style={styles.infoText}>Select account type</H2>
              <Button
                large 
                primary 
                block 
                style={[styles.btn, styles.btnPrimary]}
                onPress={() => this.setAccountType("Receiver")}>
                <Text style={styles.btnText} uppercase>Care Receiver</Text>
              </Button>
              <Button
                large 
                light 
                block 
                style={[styles.btn, styles.btnSecondary]}
                onPress={() => this.setAccountType("Provider")}>
                <Text style={[styles.btnText, styles.btnSecondaryText]} uppercase>Care Provider</Text>
              </Button>
            </View>
          </If>
          <If condition={accountType !== false}>
            <View  style={styles.formContainer}>
              <SignupForm 
                goBack={this._goBack} 
                initialValues={{accountType}}
                />
            </View>
          </If>
        </Content>
      </Container>
    )
  }
}

export default SignUp;