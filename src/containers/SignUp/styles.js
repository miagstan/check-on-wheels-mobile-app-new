const React = require("react-native");
const { StyleSheet, Platform } = React;

import { Metrics, Colors, Fonts } from '../../Themes/';

export default {
  container: {
    backgroundColor: "#FFF",
  },
  content: {
    
    //margin: 30,
    backgroundColor: Colors.themeBg,
  },
  header: {
    backgroundColor: Colors.fireLight,
    borderBottomColor: Colors.cGreyLight,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 50,
    paddingTop: Metrics.screenHeight / 3.5,
  },
  btn: {
    borderRadius: null,
    marginBottom: 20,
  },
  btnText: {
    fontFamily: "Mosk-Medium-500",
    fontSize: 20,
  },
  btnPrimary: {
    backgroundColor: Colors.fireLight,
  },
  btnSecondary: {
    backgroundColor: Colors.snow,
  },
  btnSecondaryText: {
    color: Colors.fireLight,
  },
  headerColor: {
    color: Colors.snow,
  },
  textFont: {
    fontFamily: "Mosk-Medium-500",
  },
  infoText: {
    fontFamily: "Mosk-Normal-400",
    color: Colors.cBlack,
    marginBottom: 30,
  },
  formContainer: {
    padding: 30,
  },
  termsInfo: {
    fontSize: 14,
    alignItems: 'center',
    textAlign: 'center',
    color: Colors.cBlack,
  },
};