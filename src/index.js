import React from "react";
import { StyleProvider } from 'native-base';
import { Provider } from 'react-redux';

import ErrorBoundary from './ErrorBoundary';
import getTheme from '../native-base-theme/components';
import platform from '../native-base-theme/variables/platform';

import RootContainer from './RootContainer'

import createStore from './Redux';

// create our store
const store = createStore();

export default class App extends React.Component {
  render() {
    return( 
      <ErrorBoundary>
        <Provider store={store}>
          <StyleProvider style={getTheme(platform)}>
            <RootContainer />
          </StyleProvider>
        </Provider>
      </ErrorBoundary>
    );
  }
}
