const React = require("react-native");
const { StyleSheet, Platform, StatusBar } = React;

export default {
    root: {
        marginTop: Platform.OS === 'android' ? StatusBar.currentHeight : null,
    }
}